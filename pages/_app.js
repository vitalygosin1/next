import '../styles/globals.css'

function MyApp({ Component, pageProps }) {

  return <Component
      theme='default'
      {...pageProps} />
}

export default MyApp
