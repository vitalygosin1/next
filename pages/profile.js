import nookies from 'nookies';
export const getServerSideProps =(context) => {
    const cookies = nookies.get(context);
    const user ='user' in cookies ? cookies.user : null;
    if (!user){
        // return {   code 200
        //     redirect:{
        //         destination: '/blog'
        //     }
        // }
        context.res.writeHead(301, {Location: '/blog'});
        context.res.end();
    }
    return {
        props:{
            user,
        }
    }
}

const Profile =(props) => {
    const {user}= props;
    const userJsx = user && (
        <p>Wellcom {user}</p>
    );
    return (
        <>
            <h1> Profile page</h1>
            {userJsx}
        </>
    )
}
export default Profile;